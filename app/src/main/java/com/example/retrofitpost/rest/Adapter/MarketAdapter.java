package com.example.retrofitpost.rest.Adapter;

import com.example.retrofitpost.rest.Constants.ApiConstants;
import com.example.retrofitpost.rest.model.Post;
import com.example.retrofitpost.rest.service.MarketService;

import java.util.List;

import retrofit2.Call;

public class MarketAdapter extends BaseAdapter implements MarketService {

    private MarketService marketService;

    public MarketAdapter(){

        super(ApiConstants.BASE_POST_URL);
        marketService=createService(MarketService.class);
    }

    @Override
    public Call<Post> InsertPost(Post post) {
        return marketService.InsertPost(post);
    }


    @Override
    public Call<List<Post>> getPosts () {
        return marketService.getPosts();
    }
}
