package com.example.retrofitpost;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.retrofitpost.rest.Adapter.MarketAdapter;
import com.example.retrofitpost.rest.model.Post;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.POST;

public class MainActivity extends AppCompatActivity {

    EditText title,description,urlImage;
    Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title=findViewById(R.id.title);
        description=findViewById(R.id.description);
        urlImage=findViewById(R.id.urlImage);
        enviar=findViewById(R.id.enviar);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostPosts();
            }
        });

    }
    // enviar

       private void PostPosts(){
        MarketAdapter adapter =new MarketAdapter();
        Call<Post> call=adapter.InsertPost(
                new Post(
                        title.getText().toString(),
                        description.getText().toString()
                        ,"https://japanweekend.com/madrid/wp-content/uploads/sites/8/2019/05/jrpalmer.jpg"
    ));

        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }

}
